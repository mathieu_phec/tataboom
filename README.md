# Tataboom

Tataboom est un projet DYI de trigger pour batterie

## Requis

* [Hairless MIDI](https://projectgus.github.io/hairless-midiserial/)
* [Loop Midi](https://www.tobias-erichsen.de/software/loopmidi.html)

## Code

Le code pour arduino se trouve dans le répertoire arduino