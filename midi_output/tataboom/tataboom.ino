/* Tataboom
version 0.1
Code Midi see https://www.edb.gov.hk/attachment/tc/curriculum-development/kla/arts-edu/nss/gm_drumlist_8050.pdf
Bass Drum : 36
Snare : 38
*/

//Setup
int noteOn = 144;
int DRUM_BASS[2] = {A0, 36};
int SNARE[2] = {A1, 38};
int HIT_HAT[2] = {A2, 42}; //Close hit hat
int threshold = 50;//anything over fifty means we've hit the piezo

void setup(){
  Serial.begin(9600);
}

void loop(){
  //Get value of Piezo
  int piezoVal_DRUM_BASS = analogRead(DRUM_BASS[0]);
  int piezoVal_SNARE = analogRead(SNARE[0]);
  int piezoVal_HIT_HAT = analogRead(HIT_HAT[0]);

  //Check for Drum Bass
  if (piezoVal_DRUM_BASS>threshold){
    DoTheJob(piezoVal_DRUM_BASS, DRUM_BASS[0], DRUM_BASS[1]);
  }

  //Check for Snare
  if (piezoVal_SNARE>threshold){
    DoTheJob(piezoVal_SNARE, SNARE[0], SNARE[1]);
  }

  //Check for HIT_HAT
  if (piezoVal_HIT_HAT>threshold){
    DoTheJob(piezoVal_HIT_HAT, HIT_HAT[0], HIT_HAT[1]);
  }
}

//Function

//Do the job
//Need to change this function name's :(
void DoTheJob(int piezoVal, int piezoAnalog, int instruMidiNote) {
  //Get max value
  int maxPiezoVal = getMaxVal(piezoVal, piezoAnalog);
  //Get velocity
  byte velocity = map(maxPiezoVal, 0, 1023, 50, 127);//velocity between 50 and 127 based on max val from piezo
  //Send MIDI Message with velocity (note on)
  MIDImessage(noteOn, instruMidiNote, velocity);
  delay(100);
  //Send MIDI Message with 0 velocity (like note off)
  MIDImessage(noteOn, instruMidiNote, 0);
}

//send MIDI message
void MIDImessage(byte command, byte data1, byte data2) {
  Serial.write(command);
  Serial.write(data1);
  Serial.write(data2);
}

//get Maximum value
int getMaxVal(int lastVal, int piezoPart){
  int currentVal = analogRead(piezoPart);
  while (currentVal>lastVal){
    lastVal = currentVal;
    currentVal = analogRead(piezoPart);
  }
  return lastVal;
}
